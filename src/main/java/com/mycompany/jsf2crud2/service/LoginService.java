package com.mycompany.jsf2crud2.service;

public interface LoginService {
	
	 boolean validate(String username, String password);

}
