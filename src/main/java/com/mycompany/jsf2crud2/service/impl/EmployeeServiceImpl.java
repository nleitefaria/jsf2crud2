package com.mycompany.jsf2crud2.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.mycompany.jsf2crud2.dao.EmployeeDAO;
import com.mycompany.jsf2crud2.dto.EmployeeDTO;
import com.mycompany.jsf2crud2.entity.Employee;
import com.mycompany.jsf2crud2.service.EmployeeService;

public class EmployeeServiceImpl implements EmployeeService {
	
	@Inject
	EmployeeDAO employeeDAO;
	
	public List<EmployeeDTO> findAll()
	{
		List<EmployeeDTO> ret = new ArrayList<EmployeeDTO>();		
		for(Employee employee : employeeDAO.findAll())
		{
			ret.add(new EmployeeDTO(employee.getId(), employee.getBirthDate(), employee.getFirstName(), employee.getLastName(), employee.getGender(), employee.getHireDate()));
		}
		return ret;
	}
	
	

}
