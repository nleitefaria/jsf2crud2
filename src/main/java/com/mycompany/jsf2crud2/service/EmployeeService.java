package com.mycompany.jsf2crud2.service;

import java.util.List;

import com.mycompany.jsf2crud2.dto.EmployeeDTO;

public interface EmployeeService {
	
	List<EmployeeDTO> findAll();

}
