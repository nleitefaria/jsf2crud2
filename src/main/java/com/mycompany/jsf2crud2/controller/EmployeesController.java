package com.mycompany.jsf2crud2.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import com.mycompany.jsf2crud2.dto.EmployeeDTO;
import com.mycompany.jsf2crud2.service.EmployeeService;

@ManagedBean
public class EmployeesController 
{
	private static final Logger logger = Logger.getLogger(EmployeesController.class);

	@Inject 
	EmployeeService employeeService;
	
	private List<EmployeeDTO> employeesList;

	public EmployeesController() {
		
	}

	public List<EmployeeDTO> getEmployeesList() {
		System.out.println("@getEmployeesList");
		employeesList = employeeService.findAll();
		return employeesList;
	}

	public void setEmployeesList(List<EmployeeDTO> employeesList) {
		this.employeesList = employeesList;
	}
	
	
	
	
	

}
