package com.mycompany.jsf2crud2.dao;

public interface LoginDAO {
	
	boolean validate(String username, String password);

}
