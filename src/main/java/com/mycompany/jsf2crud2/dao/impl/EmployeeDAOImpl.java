package com.mycompany.jsf2crud2.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.mycompany.jsf2crud2.dao.EmployeeDAO;
import com.mycompany.jsf2crud2.entity.Employee;
import com.mycompany.jsf2crud2.util.HibernateUtil;

public class EmployeeDAOImpl implements EmployeeDAO
{
	public EmployeeDAOImpl() 
	{
	}

	public List<Employee> findAll() {
		Session session = HibernateUtil.getSessionFactory().openSession();
    	org.hibernate.Transaction tx = null;
        List<Employee> customerList = null;
        try 
        {
        	tx = session.beginTransaction();
            Query q = session.createQuery ("from Employee as employee");
            customerList = (List<Employee>) q.list();            
        } 
        catch (RuntimeException e) 
        {
            if (tx != null) 
            {
                tx.rollback();
            }
            e.printStackTrace();
        } 
        finally 
        {
            session.close();
        }       
        return customerList;
	}

}
