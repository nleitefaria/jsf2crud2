package com.mycompany.jsf2crud2.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.mycompany.jsf2crud2.dao.LoginDAO;
import com.mycompany.jsf2crud2.entity.User;
import com.mycompany.jsf2crud2.util.HibernateUtil;

public class LoginDAOImpl implements LoginDAO 
{
	Session session = null;

	public LoginDAOImpl() 
	{
		this.session = HibernateUtil.getSessionFactory().getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	public boolean validate(String username, String password)
	{
				
		List<User> userList = null;
        try 
        {  
        	org.hibernate.Transaction tx = session.beginTransaction();
            Query q = session.createQuery ("from User where username=:username and password=:password");
            q.setParameter("username", username);
            q.setParameter("password", password);
            userList = (List<User>) q.list();            
            if(userList.size() > 0)
            {
                return true;               
            }           
        } catch (Exception e) 
        {
            e.printStackTrace();
        }       
		return false;
	}


}
