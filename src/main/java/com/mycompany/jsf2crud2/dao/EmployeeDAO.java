package com.mycompany.jsf2crud2.dao;

import java.util.List;

import com.mycompany.jsf2crud2.entity.Employee;

public interface EmployeeDAO {
	
	List<Employee> findAll();

}
